## path
* require('nyks/path/which'(bin)
Search for a binary in env PATH

* require('nyks/path/extend')( path[,path2, ..]);
Extend system PATH with new directories


* require('nyks/path/url')( file_path);
Return a path with a file:// scheme syntax

