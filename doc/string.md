
## String

* require('nyks/string/chunk')(basestr, chunksize)
Split a string into chunk of specified size.

* require('nyks/string/replaces')(dict)
Replace key => value in current string

* require('nyks/string/rreplaces')(dict)
Recursive (iterative) replaces


* require('nyks/string/stripEnd')(str)
Return trimmed string of "str" if present (else, leave untouched)

* require('nyks/string/rot13')()
Rot13 of current string