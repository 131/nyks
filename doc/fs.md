
## fs
* require('nyks/fs/deleteFolderRecursive')(path);
Recursive folder deletion

* require('nyks/fs/md5File')(file_path, callback)
* require('nyks/fs/md5FileSync')(file_path)
Return md5 checksum of a file

* require('nyks/fs/filesizeSync')(path);
* require('nyks/fs/filemtimeSync')(path);
* require('nyks/fs/isFileSync')(path)
* require('nyks/fs/isDirectorySync')(path)

* require('nyks/fs/tmppath')(ext)
Return a unique file path in OS temp dir

* require('nyks/fs/getFolderSize')(path)
Return a folder Size

